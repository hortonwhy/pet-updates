import "reflect-metadata"
import { DataSource } from "typeorm"
import { Owner } from "./entity/Owner"
import { Update } from "./entity/Update"
import 'dotenv/config'

export const AppDataSource = new DataSource({
    type: "postgres",
    host: process.env.HOST,
    port: parseInt(process.env.PORT),
    username: process.env.USERNAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    synchronize: false,
    logging: false,
    entities: [Owner, Update],
    migrations: [],
    subscribers: [],
})

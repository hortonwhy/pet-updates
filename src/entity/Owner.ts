import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class Owner {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  firstname: string

  @Column()
  lastname: string

  @Column()
  created_at: Date
}

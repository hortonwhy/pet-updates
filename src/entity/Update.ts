import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class Update {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    description: string

    @Column()
    owner_id: number

    @Column()
    date: string

    @Column()
    time: string

}

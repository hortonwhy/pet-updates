import { AppDataSource } from "./data-source"
import { Update } from "./entity/Update"
import { Owner } from "./entity/Owner"
import { Between } from "typeorm"
import { formatISO9075 } from 'date-fns'
import express, { Request, Response } from "express";
import dateFormat, { masks } from "dateformat";
import cors from 'cors';

const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cors());

AppDataSource.initialize().then(async () => {

  /*
  console.log("Inserting a new user into the database...")
  const owner = new Owner();
  owner.firstname = "Anabell";
  owner.lastname = "Thomas";
  owner.created_at = new Date();

  await AppDataSource.manager.save(owner);

  const update = new Update();
  update.name = "Bones";
  update.description = "fed bones dry dog food";
  update.owner_id = 1;
  let now = new Date();
  dateFormat(now);
  update.date = String(now);
  update.created_at = new Date();

  await AppDataSource.manager.save(update);
  */

  //console.log("Loading users from the database...")
  //const people = await AppDataSource.manager.find(Person)
  //console.log("Loaded users: ", people)


}).catch(error => console.log(error))


app.get('/', async (request: Request, response: Response) => {

  let page:number | any = request.query.page;
  if (typeof page !== "string" || parseInt(page) < 0) {
    page = 0;
  }

  const today = new Date();
  const date = new Date();
  date.setDate(-1);
  dateFormat(date);
  dateFormat(today);

  const updateRepo = AppDataSource.getRepository(Update);
  try {
  const result = await updateRepo.find({
    where: {
      owner_id: 1,
    },
    order: {
      id: "DESC",
    },
    take: 5,
    skip: 5 * page,
  });
  console.log(request.url);
  console.log(request.ip);
  response.set('Access-Control-Allow-Origin', '*');
  response.json(result);
  }
  catch(error) {
    console.log(error);
  }
    
  })

app.post('/addUpdate', async (req: Request, res: Response) => {
  console.log(req.body);
  const update = new Update();
  update.name = req.body.name;
  update.description = req.body.description;
  update.owner_id = req.body.owner_id;
  //let time_now = new Date();
  //dateFormat(time_now);
  //update.date = String(time_now);
  update.date = formatISO9075(new Date(), {representation: 'date'})
  update.time = formatISO9075(new Date(), {representation: 'time'} );

  try {
  await AppDataSource.manager.save(update);
  res.status(201).send("Successfully added update to database");
  }
  catch(error) {
    throw(error);
  }




});

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
